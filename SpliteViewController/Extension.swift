//
//  Extension.swift
//  SpliteViewController
//
//  Created by SungJaeLEE on 2016. 11. 14..
//  Copyright © 2016년 SungJaeLEE. All rights reserved.
//

import UIKit

let imageCahe = NSCache<NSString, AnyObject>()

extension UIImageView
{
    fileprivate struct StringKeys {
        static var imageUrl = "cached_image"
    }
    
    var imageUrlString: String? {
        get {
            return objc_getAssociatedObject(self, &StringKeys.imageUrl) as? String
        }
        
        set {
            if let newValue = newValue {
                objc_setAssociatedObject(self, &StringKeys.imageUrl, newValue as String?, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
        }
    }
    
    func fetchImageUsingUrlString(urlString: String , completionHandler: @escaping (_ success: Bool)->() ) {
        imageUrlString = urlString
        
        let url = NSURL(string: urlString)
        
        image = nil
        
        if let imageFromCache = imageCahe.object(forKey: urlString as NSString) as? UIImage {
            self.image = imageFromCache
            return completionHandler(true)
        }
      
        URLSession.shared.dataTask(with: url! as URL) { (data, response, error) in
            
            if error != nil {
                print(error!)
                return completionHandler(false)
            }
            
            DispatchQueue.main.async {
                let imageToCache = UIImage(data: data!)
                
                if self.imageUrlString == urlString {
                    self.image = imageToCache
                }
                
                imageCahe.setObject(imageToCache!, forKey: urlString as NSString)
                return completionHandler(true)
            }
            
            
        }.resume()
      
    }
}
