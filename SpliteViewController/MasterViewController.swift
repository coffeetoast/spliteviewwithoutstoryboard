//
//  ViewController.swift
//  SpliteViewController
//
//  Created by SungJaeLEE on 2016. 11. 13..
//  Copyright © 2016년 SungJaeLEE. All rights reserved.
//

import UIKit

class MasterViewController: UITableViewController, UISplitViewControllerDelegate {
    fileprivate let cellId = "cellId"
    fileprivate var collapseDetailViewController = true
    
    var menuItem = ["first","second","third"]
    var urlItem = ["https://koenig-media.raywenderlich.com/uploads/2017/02/05-Cards-240x320.png","http://r.ddmcdn.com/s_f/o_1/cx_633/cy_0/cw_1725/ch_1725/w_720/APL/uploads/2014/11/too-cute-doggone-it-video-playlist.jpg","http://weknowyourdreams.com/images/dog/dog-01.jpg"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        tableView.register(masterViewCell.self, forCellReuseIdentifier: cellId)
        //tableView.allowsMultipleSelection = true
        splitViewController?.delegate = self
    }

    // MARK: - UITableViewDataSource................
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItem.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! masterViewCell
        
        cell.menuLabel.text = menuItem[indexPath.row]
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //let selectedCell = tableView.cellForRow(at: indexPath)!
        //selectedCell.contentView.backgroundColor = UIColor.purple
        
        let detailViewController = DetailViewController()
        let nDVC = UINavigationController(rootViewController: detailViewController)
        
        detailViewController.navigationItem.title = menuItem[indexPath.row]
        detailViewController.activityIndicatorView.startAnimating()
        detailViewController.imageView.alpha = 0
        detailViewController.imageView.fetchImageUsingUrlString(urlString: urlItem[indexPath.row]) {
            success in
            if success {
                detailViewController.activityIndicatorView.stopAnimating()
                UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: { 
                    detailViewController.imageView.alpha = 1
                }, completion: { (success) in
                    
                })
                
            }
        }
        
        splitViewController?.showDetailViewController(nDVC, sender: nil)
        print("selected")
        collapseDetailViewController = false

    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let selectedCell = tableView.cellForRow(at: indexPath)!
        selectedCell.contentView.backgroundColor = UIColor.clear
        print("deeeeeselected")
    }
    
    //MARK: - UISplitViewControllerDelegate .............
    func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController: UIViewController, onto primaryViewController: UIViewController) -> Bool {
        return collapseDetailViewController
    }
    
}

class masterViewCell: UITableViewCell {
    let menuLabel: UILabel = {
        let mLabel = UILabel()
        mLabel.translatesAutoresizingMaskIntoConstraints = false
        //mLabel.backgroundColor = UIColor.gray
        return mLabel
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        //backgroundColor = UIColor.red
        addSubview(menuLabel)
        
        menuLabel.leftAnchor.constraint(equalTo: self.contentView.leftAnchor, constant: 8).isActive = true
        menuLabel.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        menuLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        menuLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        
        
        
    }
}






